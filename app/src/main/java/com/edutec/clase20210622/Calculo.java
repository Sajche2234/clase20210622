package com.edutec.clase20210622;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.widget.TextView;

public class Calculo extends AppCompatActivity {
    private TextView txtAnio, txtMeses, txtSemanas, txtDias;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_calculo);
        txtAnio = findViewById(R.id.txtAnio);
        txtMeses = findViewById(R.id.txtMeses);
        txtSemanas = findViewById(R.id.txtSemanas);
        txtDias =findViewById(R.id.txtDias);
        this.obtener();
    }
    private void obtener(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null){
            String anio = bundle.getString("setAnio");
            int nAnio = Integer.parseInt(anio);
            if ((nAnio % 4 == 0) && ((nAnio % 100 != 0) || (nAnio % 400 == 0))) {
                txtAnio.setText("BISIESTO");
                txtDias.setText("366 días.");
            } else {
                txtAnio.setText("AÑO NORMAL");
                txtDias.setText("365 días.");
            }
            txtMeses.setText("12 meses");
            txtSemanas.setText("52 semanas.");
        }
    }
}