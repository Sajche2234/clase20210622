package com.edutec.clase20210622;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText txtAnio;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        txtAnio = findViewById(R.id.txtAnio);
    }

    public void enviarDato(View view) {
        String anio = txtAnio.getText().toString();
        if(!anio.isEmpty()){
            Intent calcular = new Intent(this, Calculo.class);
            calcular.putExtra("setAnio", anio);
            startActivity(calcular);
        } else {
            Toast.makeText(this, "Debe de ingresar un año.", Toast.LENGTH_SHORT).show();
        }
    }
}